


<cfscript>

	function kthPercentile (k, data) {
		if (k > 1) {
			k = k / 100;
		}
		arraySort(data, "numeric");
		var kth = k * arrayLen(data);
		
		if (kth == ceiling(kth)) {
			//its a whole number
			return data[kth];
		} else {
			return ( data[int(kth)] + data[ceiling(kth)] ) / 2;
		}

	}

	javaSystem = createObject("java", "java.lang.System");

	function nanotime () {
		return javaSystem.nanoTime();
	}

	//setup
	key = "sess_" & hash("sessionkey", "md5");

	if (isNull(url.size) || !isNumeric(url.size)) {
		url.size = 4;
	}

	filename = "./txt/" & url.size & ".txt";

	if (!fileExists(expandPath(filename))) {
		filename = "./txt/128.txt";
	}

	inputData = fileReadBinary(expandPath(filename));

	obj = new Obj();
	obj.setData(inputData);

	if (isNull(url.times) || !isNumeric(url.times) || url.times < 0 || url.times > 1000000) {
		url.times = 500;
	}

	function runTest (times, data, f) {
		var out = {
			  sum: 0
			, min: 999999
			, max: 0
			, raw: []
			, length: len(data)
		};	

		for (i = 1; i <= times; i++) {


			tc = nanotime();

			f(data);
				
			time = (nanotime() - tc) / 1000000;
			arrayAppend(out.raw, time);
			out.sum += time;
			if (time < out.min) out.min = time;
			if (time > out.max) out.max = time;

		}

		out.avg = (out.sum / times);
		out.k50 = kthPercentile(50, out.raw);
		out.k90 = kthPercentile(90, out.raw);
		out.k95 = kthPercentile(95, out.raw);
		out.k98 = kthPercentile(98, out.raw);
		out.k99 = kthPercentile(99, out.raw);
		return out;
	}

	//the actual tests

	redisTimes = runTest(url.times, inputData, function(data) {

		data = binaryEncode(objectSave(obj), "base64");

		application.redlock.lock("set:test", 250, function(err, lock) {
			if (len(err)) throw(err);

			application.redis.set(key, data);				
			
			lock.unlock();
		});
	});

	sessionTimes = runTest(url.times, inputData, function(data) {

		//just set the object
		session.data[key] = obj;

		//serialize the object same as we would in redis
		//session.data[key] = binaryEncode(objectSave(obj), "base64");

		//lock and serialize the data
		/*
		lock name="set:test" timeout=200 throwontimeout=true {
			session.data[key] = binaryEncode(objectSave(obj), "base64");
		}
		*/	
	});


	overallMax = max(redisTimes.max, sessionTimes.max);

</cfscript>



	<html>
	  <head>
	    <script type="text/javascript"
	          src="https://www.google.com/jsapi?autoload={
	            'modules':[{
	              'name':'visualization',
	              'version':'1',
	              'packages':['corechart']
	            }]
	          }"></script>

	    

	    <style>
	    th, td {
	    	text-align: right;
	    	
	    	padding: 0;
	    }

	    td {
	    	border: 1px solid #ccc;
	    }

	    .rediscolor {
	    	background-color: #FD8E8E;
	    }

	    .sessioncolor {
	    	background-color: #8EFDAB;
	    }

	    .diffcolor {
	    	background-color: #FFFFA7;
	    }
	    </style>
	  </head>
	  <body>
	    

		  
	<cfoutput>

		<form action="index.cfm" method="get">
			Times: <input name="times" type="number" min="0" max="10000" value="#url.times#" /> 
			Size: 
			<select name="size">
				<option value="0.5" <cfif url.size EQ "0.5">selected="true"</cfif>>1/2 kb</option>
				<option value="1" <cfif url.size EQ "1">selected="true"</cfif>>1 kb</option>
				<option value="2" <cfif url.size EQ "2">selected="true"</cfif>>2 kb</option>
				<option value="4" <cfif url.size EQ "4">selected="true"</cfif>>4 kb</option>
				<option value="8" <cfif url.size EQ "8">selected="true"</cfif>>8 kb</option>
				<option value="16" <cfif url.size EQ "16">selected="true"</cfif>>16 kb</option>
				<option value="32" <cfif url.size EQ "32">selected="true"</cfif>>32 kb</option>
				<option value="64" <cfif url.size EQ "64">selected="true"</cfif>>64 kb</option>
				<option value="128" <cfif url.size EQ "128">selected="true"</cfif>>128 kb</option>
				<option value="256" <cfif url.size EQ "256">selected="true"</cfif>>256 kb</option>
				<option value="512" <cfif url.size EQ "512">selected="true"</cfif>>512 kb</option>
				<option value="1024" <cfif url.size EQ "1024">selected="true"</cfif>>1024 kb</option>
				<option value="5120" <cfif url.size EQ "5120">selected="true"</cfif>>5120 kb</option>
			</select>
			<input type="submit">
		</form>	

		<table width="900" align="center">
			<tr>
				<th>#url.times# / ~#ceiling(sessionTimes.length / 1024)# kb.</th>  
				<th>avg</th>  
				<th>min</th> 
				<th>median</th>  
				<th>90th %</th>  
				<th>95th %</th>  
				<th>98th %</th>  
				<th>99th %</th> 
				<th>max</th> 
			</tr>
			<tr class="rediscolor">
				<th>Redis</th>  
				<td>#numberFormat(redisTimes.avg, "0.999")# ms</td>  
				<td>#numberFormat(redisTimes.min, "0.999")# ms</td> 
				<td>#numberFormat(redisTimes.k50, "0.999")# ms</td>  
				<td>#numberFormat(redisTimes.k90, "0.999")# ms</td>  
				<td>#numberFormat(redisTimes.k95, "0.999")# ms</td>  
				<td>#numberFormat(redisTimes.k98, "0.999")# ms</td>  
				<td>#numberFormat(redisTimes.k99, "0.999")# ms</td>   
				<td>#numberFormat(redisTimes.max, "0.999")# ms</td> 
			</tr>
			<tr class="sessioncolor">
		  		<th>Session</th>  
		  		<td>#numberFormat(sessionTimes.avg, "0.999")# ms</td>  
		  		<td>#numberFormat(sessionTimes.min, "0.999")# ms</td> 
		  		<td>#numberFormat(sessionTimes.k50, "0.999")# ms</td>  
		  		<td>#numberFormat(sessionTimes.k90, "0.999")# ms</td>  
		  		<td>#numberFormat(sessionTimes.k95, "0.999")# ms</td>  
		  		<td>#numberFormat(sessionTimes.k98, "0.999")# ms</td>  
		  		<td>#numberFormat(sessionTimes.k99, "0.999")# ms</td>   
		  		<td>#numberFormat(sessionTimes.max, "0.999")# ms</td> 
		  	</tr>

		  	<cfscript>
			  	diff = {
			  		avg: redisTimes.avg - sessionTimes.avg,
					min: redisTimes.min - sessionTimes.min,
					k50: redisTimes.k50 - sessionTimes.k50,
					k90: redisTimes.k90 - sessionTimes.k90,
					k95: redisTimes.k95 - sessionTimes.k95,
					k98: redisTimes.k98 - sessionTimes.k98,
					k99: redisTimes.k99 - sessionTimes.k99,
					max: redisTimes.max - sessionTimes.max
			  	};

		  	</cfscript>	

			<tr class="diffcolor">
		  		<th>Diff</th>  
		  		<td>#numberFormat(diff.avg, "0.999")# ms</td>  
		  		<td>#numberFormat(diff.min, "0.999")# ms</td> 
		  		<td>#numberFormat(diff.k50, "0.999")# ms</td>  
		  		<td>#numberFormat(diff.k90, "0.999")# ms</td>  
		  		<td>#numberFormat(diff.k95, "0.999")# ms</td>  
		  		<td>#numberFormat(diff.k98, "0.999")# ms</td>  
		  		<td>#numberFormat(diff.k99, "0.999")# ms</td>   
		  		<td>#numberFormat(diff.max, "0.999")# ms</td> 
		  	</tr>
		  	<tr class="diffcolor">
		  		<th>Relative Diff</th>  
		  		<td>#numberFormat((redisTimes.avg / sessionTimes.avg), "0.9")#x</td>  
		  		<td>#numberFormat((redisTimes.min / sessionTimes.min), "0.9")#x</td> 
		  		<td>#numberFormat((redisTimes.k50 / sessionTimes.k50), "0.9")#x</td>  
		  		<td>#numberFormat((redisTimes.k90 / sessionTimes.k90), "0.9")#x</td>  
		  		<td>#numberFormat((redisTimes.k95 / sessionTimes.k95), "0.9")#x</td>  
		  		<td>#numberFormat((redisTimes.k98 / sessionTimes.k98), "0.9")#x</td>  
		  		<td>#numberFormat((redisTimes.k99 / sessionTimes.k99), "0.9")#x</td>   
		  		<td>#numberFormat((redisTimes.max / sessionTimes.max), "0.9")#x</td> 
		  	</tr>
		</table>

			<script type="text/javascript">
		      google.setOnLoadCallback(drawChart1);

		      function drawChart1() {
		        var data = google.visualization.arrayToDataTable([
		          ['index', 'value']
		          <cfloop from="1" to="#arrayLen(redisTimes.raw)#" index="i">
			      	,['#i#', #redisTimes.raw[i]#]    
		          </cfloop>
		        ]);

		        var options = {
		          title: 'Request Times',
		          curveType: 'function',
		          legend: { position: 'bottom' },
		          vAxis: {
		          	maxValue: #overallMax#
		          }	
		        };

		        var chart = new google.visualization.ColumnChart(document.getElementById('ColumnChart_redisTimes'));

		        chart.draw(data, options);
		      }
		    </script>

		    
		
			<h3>Redis (redlock + set)</h3>
			<table width="900" align="center">
				<tr>
					<th>times</th>  
					<th>size</th>  
					<th>avg</th>  
					<th>min</th> 
					<th>median</th>  
					<th>90th %</th>  
					<th>95th %</th>  
					<th>98th %</th>  
					<th>99th %</th> 
					<th>max</th> 
				</tr>
				<tr class="rediscolor">
					<td>#url.times#</td>  
					<td>~#ceiling(redisTimes.length / 1024)# kb.</td>  
					<td>#numberFormat(redisTimes.avg, "0.999")# ms</td>  
					<td>#numberFormat(redisTimes.min, "0.999")# ms</td> 
					<td>#numberFormat(redisTimes.k50, "0.999")# ms</td>  
					<td>#numberFormat(redisTimes.k90, "0.999")# ms</td>  
					<td>#numberFormat(redisTimes.k95, "0.999")# ms</td>  
					<td>#numberFormat(redisTimes.k98, "0.999")# ms</td>  
					<td>#numberFormat(redisTimes.k99, "0.999")# ms</td>   
					<td>#numberFormat(redisTimes.max, "0.999")# ms</td> 
				</tr>
			</table>	 
			<div id="ColumnChart_redisTimes" style="width: 100%; height: 300px"></div>

<hr />

			<script type="text/javascript">
		      google.setOnLoadCallback(drawChart2);

		      function drawChart2() {
		        var data = google.visualization.arrayToDataTable([
		          ['index', 'value']
		          <cfloop from="1" to="#arrayLen(sessionTimes.raw)#" index="i">
			      	,['#i#', #sessionTimes.raw[i]#]    
		          </cfloop>
		        ]);

		        var options = {
		          title: 'Request Times',
		          curveType: 'function',
		          legend: { position: 'bottom' },
		          vAxis: {
		          	maxValue: #overallMax#
		          }	
		        };

		        var chart = new google.visualization.ColumnChart(document.getElementById('ColumnChart_sessionTimes'));

		        chart.draw(data, options);
		      }
		    </script>	

		  	<h3>CF Session</h3>
			<table width="900" align="center">
			  	<tr>
			  		<th>times</th>  
			  		<th>size</th>  
			  		<th>avg</th>  
			  		<th>min</th> 
			  		<th>median</th>  
			  		<th>90th %</th>  
			  		<th>95th %</th>  
			  		<th>98th %</th>  
			  		<th>99th %</th> 
			  		<th>max</th> 
			  	</tr>
			  	<tr class="sessioncolor">
			  		<td>#url.times#</td>  
			  		<td>~#ceiling(sessionTimes.length / 1024)# kb.</td>  
			  		<td>#numberFormat(sessionTimes.avg, "0.999")# ms</td>  
			  		<td>#numberFormat(sessionTimes.min, "0.999")# ms</td> 
			  		<td>#numberFormat(sessionTimes.k50, "0.999")# ms</td>  
			  		<td>#numberFormat(sessionTimes.k90, "0.999")# ms</td>  
			  		<td>#numberFormat(sessionTimes.k95, "0.999")# ms</td>  
			  		<td>#numberFormat(sessionTimes.k98, "0.999")# ms</td>  
			  		<td>#numberFormat(sessionTimes.k99, "0.999")# ms</td>   
			  		<td>#numberFormat(sessionTimes.max, "0.999")# ms</td> 
			  	</tr>
			  </table>	 
			  <div id="ColumnChart_sessionTimes" style="width: 100%; height: 300px"></div>
				
<hr />

				 

</cfoutput>

		</body>
	</html>

	